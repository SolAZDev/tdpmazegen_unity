﻿using System.Collections;
using System.Linq;
using UnityEngine;
using TheDreamersPrincess.Utils;

//<summary>
//Basic class for maze generation logic
//</summary>
public abstract class BasicMazeGenerator {
    public int RowCount { get { return mMazeRows; } }
    public int ColumnCount { get { return mMazeColumns; } }

    private int mMazeRows;
    private int mMazeColumns;
    private MazeCell[,] mMaze;
    // public Unity.Mathematics.Random rnd;
    public CryptoRandom rnd;
    public Vector2Int key = Vector2Int.zero,
    door = Vector2Int.zero,
    player = Vector2Int.zero,
    oasis = Vector2Int.zero;
    bool hasOasis;
    int dProxVal = 0;
    public bool spawnDecoration = true;
    public float DecorationRandomComparable = .5f;

    public BasicMazeGenerator(int rows, int columns) {
        mMazeRows = Mathf.Abs(rows);
        mMazeColumns = Mathf.Abs(columns);
        if (mMazeRows == 0) {
            mMazeRows = 1;
        }
        if (mMazeColumns == 0) {
            mMazeColumns = 1;
        }

        mMaze = new MazeCell[rows, columns];
        for (int row = 0; row < rows; row++) {
            for (int column = 0; column < columns; column++) {
                mMaze[row, column] = new MazeCell();
                mMaze[row, column].decoWeight = Mathf.PerlinNoise(row, column);
            }
        }
        int[] sizeCalc = { mMazeRows, mMazeColumns };
        dProxVal = sizeCalc.Max();
    }

    public abstract void GenerateMaze();

    public MazeCell GetMazeCell(int row, int column) {
        if (row >= 0 && column >= 0 && row < mMazeRows && column < mMazeColumns) {
            return mMaze[row, column];
        } else {
            Debug.Log(row + " " + column);
            throw new System.ArgumentOutOfRangeException();
        }
    }

    protected void SetMazeCell(int row, int column, MazeCell cell) {
        if (row >= 0 && column >= 0 && row < mMazeRows && column < mMazeColumns) {
            mMaze[row, column] = cell;
        } else { throw new System.ArgumentOutOfRangeException(); }
    }
    public void GenerateRND() {
        // rnd = new Unity.Mathematics.Random(Helpers.NowToInt());
        rnd = new CryptoRandom();
        hasOasis = rnd.NextBool();
        //Set Key and Door
        // key = new Vector2Int(rnd.NextInt(RowCount / 2, RowCount - 1), rnd.NextInt(0, ColumnCount / 2));
        key = new Vector2Int(rnd.NextInt(0, RowCount - 1), rnd.NextInt(0, ColumnCount - 1));
        // door = new Vector2Int(rnd.NextInt(0, RowCount / 2 + 1), rnd.NextInt(ColumnCount / 2, ColumnCount - 1));
        while (door == key) door = new Vector2Int(rnd.NextInt(0, RowCount - 1), rnd.NextInt(0, ColumnCount - 1));
        while (oasis == door || oasis == key) oasis = new Vector2Int(rnd.NextInt(0, RowCount - 1), rnd.NextInt(0, ColumnCount - 1));
        // while (player == door || player == key || player == oasis) player = new Vector2Int(rnd.NextInt(0, RowCount - 1), rnd.NextInt(0, ColumnCount - 1));


        // Holy Shit this was an endless loop generator.
        // while (key.x == door.x) {
        //     key.x = rnd.NextInt(0, RowCount - 1);
        //     door.x = rnd.NextInt(0, RowCount - 1);
        // }
        // while (key.y == door.y) {
        //     key.y = rnd.NextInt(0, ColumnCount - 1);
        //     door.y = rnd.NextInt(0, ColumnCount - 1);
        // }

        // Seeing this now, Jan 27th 2023, its using the wrong variable for the X :^)
        // while (oasis.x == 0 || oasis.x == key.x || oasis.x == door.x) { oasis.x = rnd.NextInt(1, ColumnCount - 2); }
        // while (oasis.y == 0 || oasis.y == key.y || oasis.y == door.y) { oasis.y = rnd.NextInt(1, ColumnCount - 2); }

        while (player.x == 0 || player.x == key.x || player.x == oasis.x) { player.x = rnd.NextInt(0, RowCount - 1); }
        while (player.y == 0 || player.y == key.y || player.y == oasis.y) { player.y = rnd.NextInt(0, ColumnCount - 1); }
    }
    public void TDPAddons(int row, int column, int movesAvailableCount) {
        Vector2Int cell = new Vector2Int(row, column);
        // The Dreamer's Princess - Key, Door, Chest, Decoration, Enemies and Spawners
        if (cell == key) { GetMazeCell(row, column).IsKey = true; }
        if (cell == door) { GetMazeCell(row, column).IsDoor = true; }
        if (cell == oasis) { GetMazeCell(row, column).hasOasis = true; }

        //New Version
        if (cell != key && cell != door && cell != oasis) {
            GetMazeCell(row, column).HasChest = rnd.NextDouble() > .75;
            GetMazeCell(row, column).HasDecoration = rnd.NextDouble() > (double)DecorationRandomComparable;
            GetMazeCell(row, column).hasTraps = rnd.NextDouble() > .75;
        }
        //TODO: Find cleaner way to express radius check
        if (cell != player &&
            IsInProximity(new Vector2Int(row, column), player, (int)(dProxVal / 1.5f)) &&
            !IsInProximity(new Vector2Int(row, column), player, 1)) {

            //Significantly increase chances of enemies
            bool HasEnemy = rnd.NextDouble() >= .58;
            MazeCell mazeCell = GetMazeCell(row, column);
            mazeCell.HasEnemy = HasEnemy && !mazeCell.hasOasis;
            // if (HasEnemy) MonoBehaviour.print(row + ":" + column + " has enemies!");
        }

        GetMazeCell(row, column).HasSpanwer = (rnd.NextDouble() > .6);
    }

    public bool IsInProximity(Vector2Int pos, Vector2Int reference, int maxDist = 5) {
        return (pos.x - reference.x < maxDist) && (pos.y - reference.y < maxDist);
    }

}