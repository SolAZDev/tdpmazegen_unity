﻿using System.Collections;
using UnityEngine;

public enum Direction {
    Start,
    Right,
    Front,
    Left,
    Back,
};
//<summary>
//Class for representing concrete maze cell.
//</summary>
public class MazeCell {
    public bool IsVisited = false;
    public bool WallRight = false;
    public bool WallFront = false;
    public bool WallLeft = false;
    public bool WallBack = false;
    public bool IsGoal = false;

    //The Dreamer's Princess Addons
    public bool IsKey = false;
    public bool IsDoor = false;
    public bool HasEnemy = false;
    public bool HasSpanwer = false;
    public bool HasChest = false;
    public bool HasDecoration = false;
    public bool hasOasis = false;
    public bool hasTraps = false;
    public float decoWeight = 0;

    public override string ToString() {
        return (
"WallRight: " + WallRight + " / " +
"WallFront: " + WallFront + " / " +
"WallLeft: " + WallLeft + " / " +
"WallBack: " + WallBack + " / " +
"IsGoal: " + IsGoal + " / " +
"IsKey: " + IsKey + " / " +
"IsDoor: " + IsDoor + " / " +
"HasEnemy: " + HasEnemy + " / " +
"HasSpanwer: " + HasSpanwer + " / " +
"HasChest: " + HasChest + " / " +
"HasDecoration: " + HasDecoration + " / " +
"hasOasis: " + hasOasis + " / " +
"hasTraps: " + hasTraps + " / " +
"DecorationWeight: " + decoWeight
        );
    }
}