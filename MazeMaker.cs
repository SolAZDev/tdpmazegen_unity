﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

using Apex.AI;
using Apex.AI.Components;

using TheDreamersPrincess.AI.Contexts;
using TheDreamersPrincess.Components;
using TheDreamersPrincess.Utils;
using TheDreamersPrincess.Definitions;

using MoreMountains.Feedbacks;
using UnityEngine;
//<summary>
//Game object, that creates maze and CreateObjects it in scene
//</summary>
public class MazeMaker : MonoBehaviour {
    public static MazeMaker instance;

    public enum MazeGenerationAlgorithm {
        PureRecursive,
        RecursiveTree,
        RandomTree,
        OldestTree,
        //RecursiveDivision,
    }

    [Header("Maze Settings")]
    public bool AutoGenerate = false;
    public MazeGenerationAlgorithm Algorithm = MazeGenerationAlgorithm.PureRecursive;
    public bool Nightmare = false;
    public ParticleSystem mainFog;
    public Light MainLight;
    //	public GameObject Pillar = null;
    public int Rows = 5;
    public int Columns = 5;
    public float SecondsPerCell = 30;
    public float WallHeight = 3;
    public bool AddGaps = true;
    public bool SpawnEnemies = true;
    public Player player;
    public GameStageDatabase StratumObjects = null;
    public UtilityAIComponent testAI;
    public bool RunBasicAITest = false;
    public MMF_Player EnemyHurtFeedback;
    public Renderer[] FloorTiles;
    private BasicMazeGenerator mMazeGenerator = null;
    PlayerMazeContext context;
    Transform genKey, genDoor;
    GameObject MazeHolder, DecorationHolder, EnemyHolder, SpawnerHolder;
    StratumData variation;

    float CellSize = 5;
    int MaxEnemies = 5;
    List<Transform> spawners;
    void Start() {
        instance = this;
        if (AutoGenerate) { Generate(); }
    }
    public void Generate() {
        StartCoroutine(GenerateMaze());
    }

    public Vector2 GetSpotInMazeCenter(float radius) {
        Vector2 center = new Vector2(Columns / 2, Rows / 2) * CellSize;
        Vector2 rPos = mMazeGenerator.rnd.insideUnitCircle(radius);
        // Vector2 rPos = Random.insideUnitCircle * radius;
        return new Vector2(center.x + rPos.y, center.y + rPos.y);
    }
    public Vector2 GetRandomCellPosition(float radius) {
        // Vector2 rPos = Random.insideUnitCircle * radius;
        Vector2 rPos = mMazeGenerator.rnd.insideUnitCircle(radius);
        Vector2 iPos = new Vector2(mMazeGenerator.rnd.NextInt(0, Columns), mMazeGenerator.rnd.NextInt(0, Rows));
        Vector2 mPos = iPos * CellSize;
        Vector2 fPos = new Vector2((mPos.x - (CellSize / 2)) + rPos.x, (mPos.y - (CellSize / 2)) + rPos.y);
        return fPos;
    }

    public IEnumerator GenerateMaze() {
        print("Maze Generation Start!");
        yield return new WaitWhile(() => HUDControls.instance == null);
        HUDControls.instance.ToggleLoading(true);
        yield return new WaitWhile(() => GameManager.Instance == null);
        if (PoolingSystem.instance == null) PoolingSystem.instance = new PoolingSystem();
        yield return null;

        if (GameManager.Instance.activeStratum != null) StratumObjects = GameManager.Instance.activeStratum;
        //Randomize Stratum Variant
        variation = Helpers.SelectRandom<StratumData>(StratumObjects.Stratums);
        CellSize = variation.ObjectAndThemeSettings.CellSize;
        MaxEnemies = StratumObjects.MaxEnemies;
        Rows = GameManager.Instance.rnd.NextInt(StratumObjects.MinRows, StratumObjects.MaxRows);
        Columns = GameManager.Instance.rnd.NextInt(StratumObjects.MinRows, StratumObjects.MaxRows);

        // Generate Maze
        switch (Algorithm) {
            case MazeGenerationAlgorithm.PureRecursive:
                mMazeGenerator = new RecursiveMazeGenerator(Rows, Columns);
                break;
            case MazeGenerationAlgorithm.RecursiveTree:
                mMazeGenerator = new RecursiveTreeMazeGenerator(Rows, Columns);
                break;
            case MazeGenerationAlgorithm.RandomTree:
                mMazeGenerator = new RandomTreeMazeGenerator(Rows, Columns);
                break;
            case MazeGenerationAlgorithm.OldestTree:
                mMazeGenerator = new OldestTreeMazeGenerator(Rows, Columns);
                break;
        }
        mMazeGenerator.DecorationRandomComparable = variation.DecorationSettings.DecorationRandomComparable;
        mMazeGenerator.GenerateMaze();
        yield return null;

        // foreach (var tile in FloorTiles) { tile.material = StratumObjects.FloorMatetrial; yield return null; }
        foreach (var tile in FloorTiles) { tile.material = StratumObjects.FloorMatetrial; }
        player.agent.Warp(new Vector3(mMazeGenerator.player.x, 0, mMazeGenerator.player.x) * CellSize);


        MainLight.color = variation.AtmosphericSettings.LightColor;
        MainLight.shadows = variation.AtmosphericSettings.ShadowMode;

        RenderSettings.fogDensity = variation.AtmosphericSettings.FogDensity;
        RenderSettings.fogColor = variation.AtmosphericSettings.RenderFogColor;

        RenderSettings.ambientLight = variation.AtmosphericSettings.AmbientLightColor;
        Camera.main.backgroundColor = variation.AtmosphericSettings.BackgroundColor;

        Color particleColor = variation.AtmosphericSettings.RenderFogColor;
        particleColor.a = .15f;
        mainFog.startColor = particleColor;

        // Make Hirearchy 
        MazeHolder = new GameObject("Walls & Items");
        MazeHolder.transform.position = Vector3.zero;
        MazeHolder.transform.parent = this.transform;
        yield return null;

        DecorationHolder = new GameObject("Decorations");
        DecorationHolder.transform.position = Vector3.zero;
        DecorationHolder.transform.parent = this.transform;
        yield return null;

        EnemyHolder = new GameObject("Enemies");
        EnemyHolder.transform.position = Vector3.zero;
        EnemyHolder.transform.parent = this.transform;
        yield return null;

        SpawnerHolder = new GameObject("Spawners");
        SpawnerHolder.transform.position = Vector3.zero;
        SpawnerHolder.transform.parent = this.transform;
        spawners = new List<Transform>();
        yield return null;

        // Enemy Limiter
        int EnemiesMade = 0;

        Time.timeScale = 0.01f;
        player.gameObject.SetActive(false);

        #region MazeGen
        print("Spawning Maze");
        for (int row = 0; row < Rows; row++) {
            for (int column = 0; column < Columns; column++) {
                float x = column * (CellSize + (AddGaps ? .2f : 0));
                float z = row * (CellSize + (AddGaps ? .2f : 0));
                MazeCell cell = mMazeGenerator.GetMazeCell(row, column);
                GameObject tmp, Wall;
                Vector2 rPos = Vector2.zero;
                float yRot = 0;

                #region Walls
                if (cell.WallRight) {
                    // Wall = StratumObjects.Walls[mMazeGenerator.rnd.NextInt(0, StratumObjects.Walls.Count)];
                    Wall = Helpers.SelectRandom<GameObject>(variation.ObjectAndThemeSettings.Walls);
                    tmp = Instantiate(Wall, new Vector3(x + CellSize / 2, 0, z) + Wall.transform.position, Quaternion.Euler(0, 90, 0)) as GameObject; // right
                    yield return null;
                    tmp.transform.localScale = new Vector3(CellSize, 1.5f * WallHeight, 1);
                    tmp.transform.parent = MazeHolder.transform;
                    tmp.isStatic = true;
                }
                if (cell.WallFront) {
                    // Wall = StratumObjects.Walls[mMazeGenerator.rnd.NextInt(0, StratumObjects.Walls.Count)];
                    Wall = Helpers.SelectRandom<GameObject>(variation.ObjectAndThemeSettings.Walls);
                    tmp = Instantiate(Wall, new Vector3(x, 0, z + CellSize / 2) + Wall.transform.position, Quaternion.Euler(0, 0, 0)) as GameObject; // front
                    yield return null;
                    tmp.transform.localScale = new Vector3(CellSize, 1.5f * WallHeight, 1);
                    tmp.transform.parent = MazeHolder.transform;
                    tmp.isStatic = true;
                }
                if (cell.WallLeft) {
                    // Wall = StratumObjects.Walls[mMazeGenerator.rnd.NextInt(0, StratumObjects.Walls.Count)];
                    Wall = Helpers.SelectRandom<GameObject>(variation.ObjectAndThemeSettings.Walls);
                    tmp = Instantiate(Wall, new Vector3(x - CellSize / 2, 0, z) + Wall.transform.position, Quaternion.Euler(0, 270, 0)) as GameObject; // left
                    yield return null;
                    tmp.transform.localScale = new Vector3(CellSize, 1.5f * WallHeight, 1);
                    tmp.transform.parent = MazeHolder.transform;
                    tmp.isStatic = true;
                }
                if (cell.WallBack) {
                    // Wall = StratumObjects.Walls[mMazeGenerator.rnd.NextInt(0, StratumObjects.Walls.Count)];
                    Wall = Helpers.SelectRandom<GameObject>(variation.ObjectAndThemeSettings.Walls);
                    tmp = Instantiate(Wall, new Vector3(x, 0, z - CellSize / 2) + Wall.transform.position, Quaternion.Euler(0, 180, 0)) as GameObject; // back
                    yield return null;
                    tmp.transform.localScale = new Vector3(CellSize, 1.5f * WallHeight, 1);
                    tmp.transform.parent = MazeHolder.transform;
                    tmp.isStatic = true;
                }
                #endregion

                #region Other Objects
                if (cell.HasChest) { //Fragments
                    tmp = GameObject.Instantiate(variation.ObjectAndThemeSettings.Chest.obj) as GameObject;
                    DecoItemPlacer(tmp, StratumObjects.Door.config, cell, x, z, variation.ObjectAndThemeSettings.Chest.Offset, false);
                    GameManager.Instance.sessionInfo.AvailableFragments++;
                    yield return null;
                    tmp.transform.parent = MazeHolder.transform;
                    tmp.isStatic = true;
                }
                if (cell.IsKey) {
                    tmp = Instantiate(StratumObjects.Key) as GameObject;
                    DecoItemPlacer(tmp, StratumObjects.Door.config, cell, x, z, variation.ObjectAndThemeSettings.Key.Offset, false);
                    yield return null;
                    // print(row + ":" + column + "::" + cell.ToString());
                    genKey = tmp.transform;
                    tmp.transform.parent = MazeHolder.transform;
                }
                if (cell.IsDoor) {
                    tmp = Instantiate(StratumObjects.Door.obj) as GameObject;
                    yield return null;
                    DecoItemPlacer(tmp, StratumObjects.Door.config, cell, x, z, StratumObjects.Door.Offset, false);
                    // print(row + ":" + column + "::" + cell.ToString());
                    genDoor = tmp.transform;
                    tmp.transform.parent = MazeHolder.transform;
                    tmp.isStatic = true;
                }
                //Decorations are now their post-generation multi-pass process.
                if (StratumObjects.Traps.Count > 0 && cell.hasTraps) {
                    tmp = Instantiate(Helpers.SelectRandom<GameObject>(variation.ObjectAndThemeSettings.Traps)) as GameObject;
                    yield return null;
                    rPos = mMazeGenerator.rnd.insideUnitCircle(StratumObjects.MaxSpawnRadius);
                    tmp.transform.position += new Vector3(x + rPos.x, 0.1f, z + rPos.y);
                    tmp.transform.parent = DecorationHolder.transform;
                    tmp.isStatic = true;
                }
                if (StratumObjects.Oasis != null && cell.hasOasis && variation.ObjectAndThemeSettings.Oasis.obj != null) {
                    tmp = Instantiate(variation.ObjectAndThemeSettings.Oasis.obj) as GameObject;
                    yield return null;
                    yRot = mMazeGenerator.rnd.NextFloat(0, 360);
                    tmp.transform.position = new Vector3(x, StratumObjects.OasisHeight, z);
                    tmp.transform.rotation = Quaternion.Euler(new Vector3(StratumObjects.RotateOasis ? -90 : 0, yRot, 0));
                    tmp.transform.parent = DecorationHolder.transform;
                    tmp.isStatic = true;
                }
                if (SpawnEnemies) {
                    //Enemies and Spawners
                    if (cell.HasEnemy && StratumObjects.Enemies.Count > 0) {
                        if (EnemiesMade < MaxEnemies) {
                            for (int i = 0; i < mMazeGenerator.rnd.NextInt(1, StratumObjects.EnemiesPerCell); i++) {
                                Enemy enemy = Instantiate(Helpers.SelectRandom<Enemy>(variation.ObjectAndThemeSettings.Enemies), new Vector3(x, 0, z), Quaternion.Euler(0, 0, 0)).GetComponent<Enemy>();
                                yield return null;
                                yRot = mMazeGenerator.rnd.NextFloat(0, 360);
                                rPos = mMazeGenerator.rnd.insideUnitCircle(StratumObjects.MaxSpawnRadius);
                                enemy.transform.position += new Vector3(rPos.x, 0, rPos.y);
                                enemy.transform.rotation = Quaternion.Euler(new Vector3(-90, yRot, 0));
                                enemy.transform.parent = EnemyHolder.transform;
                                enemy.playerActor = player;
                                enemy.HurtFeedback = EnemyHurtFeedback;
                                if (enemy != null) PoolingSystem.instance.EnemyPool.Add(enemy);
                                EnemiesMade++;
                            }
                        }
                    }
                    if (cell.HasSpanwer) {
                        var tmps = Instantiate(new GameObject(), new Vector3(x, 0, z), Quaternion.Euler(0, 0, 0)) as GameObject;
                        yield return null;
                        tmps.name = "Spawner";
                        tmps.tag = "Spawner";
                        tmps.transform.parent = SpawnerHolder.transform;
                        tmps.isStatic = true;
                        tmps.transform.parent = SpawnerHolder.transform;
                        spawners.Add(tmps.transform);
                    }

                }
                #endregion
                yield return null;
            }
            yield return null;
        }
        #endregion
        yield return null;
        //For Test AI;
        if (RunBasicAITest) { context = new PlayerMazeContext(player, genKey, genDoor); }

        print("Placing Pillars");
        //Pillars
        if (StratumObjects.Pillars.Count >= 1) {
            for (int row = 0; row < Rows + 1; row++) {
                for (int column = 0; column < Columns + 1; column++) {
                    float h = mMazeGenerator.rnd.NextInt(0, 10), w = mMazeGenerator.rnd.NextInt(0, 10);
                    float o = mMazeGenerator.rnd.NextInt(0, 10), p = mMazeGenerator.rnd.NextInt(0, 10);
                    float x = column * (CellSize + (AddGaps ? .2f : 0));
                    float z = row * (CellSize + (AddGaps ? .2f : 0));
                    // if (w >= 3 && w <= 8 && o >= 7 && h >= 2 && h <= 7 && p >= 1) {
                    if (((w >= 3 && w <= 8) || o >= 7) && ((h >= 2 && h <= 7) || p >= 1)) {
                        GameObject tmp = Instantiate(StratumObjects.Pillars[mMazeGenerator.rnd.NextInt(0, StratumObjects.Pillars.Count)]) as GameObject;
                        tmp.transform.position = new Vector3(x - CellSize / 2, 0, z - CellSize / 2);
                        yield return null;
                        tmp.transform.parent = MazeHolder.transform;
                        tmp.transform.localScale = new Vector3(WallHeight, WallHeight, WallHeight);
                        tmp.isStatic = true;
                    }
                }
            }
        }

        //Decoration Second Pass
        if (variation.DecorationSettings.Passes.Count > 0) {
            for (int pass = 0; pass < variation.DecorationSettings.Passes.Count; pass++) {
                int[,] decorationsSpawned = new int[Rows, Columns];
                for (int row = 0; row < Rows; row++) {
                    for (int column = 0; column < Columns; column++) {
                        MazeCell cell = mMazeGenerator.GetMazeCell(row, column);
                        float x = column * (CellSize + (AddGaps ? .2f : 0));
                        float z = row * (CellSize + (AddGaps ? .2f : 0));
                        GameObject tmp;

                        if (cell.HasDecoration) print(row + ":" + column + ", can be decorated!");
                        if (cell.HasDecoration && decorationsSpawned[row, column] < variation.DecorationSettings.MaxDecorationsPerCell) {
                            print("Attempting to spawn Decoration!! " + decorationsSpawned[row, column] + "/" + variation.DecorationSettings.MaxDecorationsPerCell + " slots used!");
                            //Olden Code
                            // float h = mMazeGenerator.rnd.NextInt(0, 10), w = mMazeGenerator.rnd.NextInt(0, 10);
                            // float o = mMazeGenerator.rnd.NextInt(0, 10), p = mMazeGenerator.rnd.NextInt(0, 10);
                            // if (((w >= 3 && w <= 8) || o >= 7) && ((h >= 2 && h <= 7) || p >= 1))

                            //New Generator based on Stratum Variation Value
                            if (GameManager.Instance.rnd.NextInt(0, 100) > (int)variation.DecorationSettings.DecorationRandomComparable * 100) {
                                print("Decorated!!");
                                for (int i = 0; i < GameManager.Instance.rnd.NextInt(0, (variation.DecorationSettings.MaxDecorationsPerCell - decorationsSpawned[row, column])); i++) {
                                    DecorationDefinition item = Helpers.SelectRandom<DecorationDefinition>(variation.DecorationSettings.Passes[pass].Decorations);
                                    tmp = Instantiate(item.obj) as GameObject;
                                    yield return null;
                                    DecoItemPlacer(tmp, item.config, cell, x, z, item.Offset);
                                    tmp.transform.parent = DecorationHolder.transform;
                                    tmp.isStatic = true;
                                }
                            }
                        }
                    }
                }
            }
        }


        //Finalize
        yield return null;
        // player.CanMove = true;
        HUDControls.instance.minimapRender.backgroundColor = Camera.main.backgroundColor;
        float TimeLimit = (((Rows + Columns) * StratumObjects.TimePerCell) + CellSize) / GameManager.Instance.Difficulty;
        HUDControls.instance.TimeLimit = TimeLimit;
        GameManager.Instance.sessionInfo.TotalTime = TimeLimit;
        yield return null;
        print("Final Wait");
        Time.timeScale = .5f;
        yield return new WaitForSeconds(.05f); //Ensure no particles are around
        Time.timeScale = 1;
        HUDControls.instance.BGM.clip = StratumObjects.BGM;
        HUDControls.instance.BGM.Play();
        HUDControls.instance.ToggleLoading(false);
        HUDControls.instance.StartTimer();
        PoolingSystem.instance.CleanEnemyPool();
        player.gameObject.SetActive(true);
        player.ActualStart();
        if (RunBasicAITest) { testAI.enabled = true; }
        StartCoroutine(DelayRespawn());
    }
    IEnumerator DelayRespawn(float sec = 60) {
        yield return new WaitForSeconds(sec);
        StartCoroutine(EnemyRespawner());
    }
    public IAIContext GetContext(System.Guid id) {
        return context;
    }

    public void ToggleEnemies(bool toggle) {
        EnemyHolder.SetActive(toggle);
        // if (!toggle) UbhBulletManager.instance.RemoveAllBullets ();
    }

    public void DisableRandomLights(Transform wall) {
        for (int i = 0; i < wall.childCount; i++) {
            if (wall.GetChild(i).name.Contains("ight")) wall.GetChild(i).gameObject.SetActive(mMazeGenerator.rnd.NextBool());
        }
    }

    public void DecoItemPlacer(GameObject tmp, DecoSpawnConfig config, MazeCell cell, float x, float z, float Offset, bool ignoreBadRotation = true) {
        float yRot = 0;
        Vector2 rPos = Vector2.zero;
        float CellSize = variation.ObjectAndThemeSettings.CellSize;
        switch (config) {
            case DecoSpawnConfig.Random:
                yRot = mMazeGenerator.rnd.NextFloat(0, 360);
                rPos = mMazeGenerator.rnd.insideUnitCircle(StratumObjects.MaxSpawnRadius);
                tmp.transform.rotation = ignoreBadRotation ? Quaternion.Euler(new Vector3(-90, yRot, 0)) : Quaternion.Euler(new Vector3(0, yRot, 0));
                tmp.transform.position += new Vector3(x + rPos.x, 0, z + rPos.y);
                break;
            case DecoSpawnConfig.RandomLookAtCenter:
                rPos = mMazeGenerator.rnd.insideUnitCircle(StratumObjects.MaxSpawnRadius);
                yRot = Quaternion.LookRotation(new Vector3(x, 0, z), Vector3.up).eulerAngles.y;
                tmp.transform.rotation = ignoreBadRotation ? Quaternion.Euler(new Vector3(-90, yRot, 0)) : Quaternion.Euler(new Vector3(0, yRot, 0));
                tmp.transform.position += new Vector3(x + rPos.x, 0, z + rPos.y);
                break;
            case DecoSpawnConfig.Center:
                yRot = mMazeGenerator.rnd.NextFloat(0, 360);
                tmp.transform.position += new Vector3(x, 0, z);
                tmp.transform.rotation = ignoreBadRotation ? Quaternion.Euler(new Vector3(-90, yRot, 0)) : Quaternion.Euler(new Vector3(0, yRot, 0));
                break;
            case DecoSpawnConfig.NearEdgeOnlyRandomRot:
                if (cell.WallFront) rPos.y = z - CellSize + Offset; rPos.x = x - mMazeGenerator.rnd.NextFloat(-CellSize + Offset, CellSize - Offset);
                if (cell.WallBack) rPos.y = z + CellSize - Offset; rPos.x = x - mMazeGenerator.rnd.NextFloat(-CellSize + Offset, CellSize - Offset);
                if (cell.WallRight) rPos.x = x - CellSize + Offset; rPos.y = z - mMazeGenerator.rnd.NextFloat(-CellSize + Offset, CellSize - Offset);
                if (cell.WallLeft) rPos.x = x + CellSize - Offset; rPos.y = z - mMazeGenerator.rnd.NextFloat(-CellSize + Offset, CellSize - Offset);
                tmp.transform.position += new Vector3(rPos.x, 0, rPos.y);
                yRot = mMazeGenerator.rnd.NextFloat(0, 360);
                tmp.transform.rotation = ignoreBadRotation ? Quaternion.Euler(new Vector3(-90, yRot, 0)) : Quaternion.Euler(new Vector3(0, yRot, 0));
                break;
            case DecoSpawnConfig.NearEdgeOnlyLookAtCenter:
                if (cell.WallFront) rPos.y = z - CellSize + Offset; rPos.x = x - mMazeGenerator.rnd.NextFloat(-CellSize + Offset, CellSize - Offset);
                if (cell.WallBack) rPos.y = z + CellSize - Offset; rPos.x = x - mMazeGenerator.rnd.NextFloat(-CellSize + Offset, CellSize - Offset);
                if (cell.WallRight) rPos.x = x - CellSize + Offset; rPos.y = z - mMazeGenerator.rnd.NextFloat(-CellSize + Offset, CellSize - Offset);
                if (cell.WallLeft) rPos.x = x + CellSize - Offset; rPos.y = z - mMazeGenerator.rnd.NextFloat(-CellSize + Offset, CellSize - Offset);
                tmp.transform.position += new Vector3(rPos.x, 0, rPos.y);
                yRot = Quaternion.LookRotation(new Vector3(x, 0, z), Vector3.up).eulerAngles.y;
                tmp.transform.rotation = ignoreBadRotation ? Quaternion.Euler(new Vector3(-90, yRot, 0)) : Quaternion.Euler(new Vector3(0, yRot, 0));
                tmp.transform.rotation = Quaternion.Euler(new Vector3(-90, yRot, 0));
                break;
            case DecoSpawnConfig.TrueEdgeOnly:
                if (cell.WallFront) rPos.y = z - CellSize + Offset; rPos.x = x - mMazeGenerator.rnd.NextFloat(-CellSize + Offset, CellSize - Offset); tmp.transform.forward = Vector3.back;
                if (cell.WallBack) rPos.y = z + CellSize - Offset; rPos.x = x - mMazeGenerator.rnd.NextFloat(-CellSize + Offset, CellSize - Offset); tmp.transform.forward = Vector3.forward;
                if (cell.WallRight) rPos.x = x - CellSize + Offset; rPos.y = z - mMazeGenerator.rnd.NextFloat(-CellSize + Offset, CellSize - Offset); tmp.transform.forward = Vector3.left;
                if (cell.WallLeft) rPos.x = x + CellSize - Offset; rPos.y = z - mMazeGenerator.rnd.NextFloat(-CellSize + Offset, CellSize - Offset); tmp.transform.forward = Vector3.right;
                tmp.transform.position += new Vector3(rPos.x, 0, rPos.y);
                break;
        }
    }

    IEnumerator EnemyRespawner() {
        while (true && EnemyHolder.transform.childCount > 0) { //TODO: Change to While Session is playing
            yield return new WaitForSeconds(GameManager.Instance.rnd.NextInt(14, 30));
            // yield return new WaitForSeconds(2); //Only for Debugging
            print("Attempting Respawn!");
            List<Transform> availableSpawners = new List<Transform>();
            spawners.ForEach(s => {
                if ((Vector3.Distance(s.position, player.transform.position) > CellSize * 2
                && Vector3.Distance(s.position, player.transform.position) < CellSize * StratumObjects.MaxRows / 2))
                    availableSpawners.Add(s);
            });
            List<Transform> possibleSpawners = availableSpawners.OrderBy((t) => Vector3.Distance(t.position, player.transform.position)).ToList();
            Transform spawner = availableSpawners.Count > 1 ? availableSpawners[1] : availableSpawners[0];
            Enemy enemy = PoolingSystem.instance.GetEnemy();
            if (enemy != null) {
                print("Spawning " + enemy.gameObject.name + " at " + spawner.position);
                Vector2 pos = GameManager.Instance.rnd.insideUnitCircle(new Vector3(spawner.position.x, spawner.position.z), variation.GeneralMazeSettings.MaxSpawnRadius / 1.2f);
                enemy.transform.position = new Vector3(pos.x, 0, pos.y);
                //TODO: Spawn Respawn Particle
                yield return new WaitForSeconds(.25f);
                enemy.gameObject.SetActive(true);
                enemy.aiRunner.Resume();
            }
        }
    }

    public void ResultScreenMode() {
        genDoor.gameObject.SetActive(false);
        ToggleEnemies(false);
    }
}